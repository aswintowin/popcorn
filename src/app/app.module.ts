import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { AppRoutingModule } from './app-routing.module';

import { AppComponent } from './app.component';
import { HomeComponent } from './home/home.component';
import { AboutComponent } from './about/about.component';
import { ErrorComponent } from './error/error.component';
import { UserComponent } from './user/user.component';
import { SpecialpageComponent } from './specialpage/specialpage.component';
import {SearchComponent} from './search/search.component';
import { MovieComponent } from './movie/movie.component';


import {SearchService} from '../services/search.service';
import {ActivityService} from '../services/activity.service';
import { PersonComponent } from './person/person.component';
import { ListsComponent } from './lists/lists.component';
import { ListComponent } from './list/list.component';


@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    AboutComponent,
    ErrorComponent,
    UserComponent,
    SpecialpageComponent,
    SearchComponent,
    MovieComponent,
    PersonComponent,
    ListsComponent,
    ListComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    AppRoutingModule
  ],
  providers: [
    SearchService,
    ActivityService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
