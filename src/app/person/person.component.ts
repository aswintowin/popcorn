import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import {SearchService} from './../../services/search.service';
import {IPerson}  from '../../dto/Person';
import {ICredit} from '../../dto/Credit';

@Component({
  selector: 'app-person',
  templateUrl: './person.component.html',
  styleUrls: ['./person.component.css']
})
export class PersonComponent implements OnInit {
  personId: number;
  errorMessage: string;
  person: IPerson;
  credits: ICredit;

  constructor(
    private route: ActivatedRoute, 
    private router: Router,
    private searchService: SearchService) { }

  ngOnInit() {
    const param = this.route.snapshot.params['id'];
    if (param) {
      const id = +param;
      this.getPerson(id);
    }
  }

  getPerson(id: number) {
    this.searchService.getPersonDetails(id).subscribe(
      person => this.person = person,
      error => this.errorMessage = <any>error);
    
    this.searchService.getPersonCredits(id).subscribe(
    credits => this.credits = credits,
    error => this.errorMessage = <any>error);
  }
  
  onBack(): void {
    this.router.navigate(['/home']);
  }
}
