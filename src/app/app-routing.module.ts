import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { AboutComponent } from './about/about.component';
import { UserComponent } from './user/user.component';
import { ErrorComponent } from './error/error.component';
import { SpecialpageComponent } from './specialpage/specialpage.component';
import {SearchComponent} from './search/search.component';
import {MovieComponent} from './movie/movie.component';
import { PersonComponent } from './person/person.component';
import {ListsComponent} from './lists/lists.component';
import {ListComponent} from './list/list.component';

const routes: Routes = [
  {
    path: '',
    component: HomeComponent
  },
  {
    path: 'about',
    component: AboutComponent
  },
  {
    path: 'user',
    component: UserComponent
  },
  {
    path: 'specialpage',
    component: SpecialpageComponent
  },
  {
    path: 'movie/:id',
    component: MovieComponent
  },
  {
    path: 'person/:id',
    component: PersonComponent
  },
  {
    path: 'search',
    component: SearchComponent
  },
  {
    path: 'lists',
    component: ListsComponent
  },
  {
    path: 'list/:id',
    component: ListComponent
  },
  {
    path: '**',
    component: ErrorComponent
  }

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
  providers: []
})
export class AppRoutingModule { }
