import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import {SearchService} from '../../services/search.service';
import {IMovie} from '../../dto/Movie';
import {ICredit} from '../../dto/Credit';

@Component({
  selector: 'app-movie',
  templateUrl: './movie.component.html',
  styleUrls: ['./movie.component.css']
})
export class MovieComponent implements OnInit {
  movieId: any;
  movie: IMovie;
  errorMessage: string;
  credits: ICredit;

  constructor(private route: ActivatedRoute, private searchService: SearchService) {
    
  }

  ngOnInit() {
    const param = this.route.snapshot.params['id'];
    if (param) {
      const id = +param;
      this.getMovie(id);
    }
  }

  getMovie(id: number) {
    this.searchService.getMovieDetails(id).subscribe(
      movie => this.movie = movie,
      error => this.errorMessage = <any>error);
      
    this.searchService.getMovieCredits(id).subscribe(
      credits => this.credits = credits,
      error => this.errorMessage = <any>error);
  }
}
