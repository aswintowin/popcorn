import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.css']
})
export class ListComponent implements OnInit {
  listName: string;
  movies: any;

  constructor() { }

  ngOnInit() {
    this.listName = 'Batman';
    this.movies = [
      {
        "vote_count": 2295,
        "id": 268,
        "video": false,
        "vote_average": 7,
        "title": "Batman",
        "popularity": 13.414789,
        "poster_path": "/kBf3g9crrADGMc2AMAMlLBgSm2h.jpg",
        "original_language": "en",
        "original_title": "Batman",
        "genre_ids": [
          14,
          28
        ],
        "backdrop_path": "/2blmxp2pr4BhwQr74AdCfwgfMOb.jpg",
        "adult": false,
        "overview": "The Dark Knight of Gotham City begins his war on crime with his first major enemy being the clownishly homicidal Joker, who has seized control of Gotham's underworld.",
        "release_date": "1989-06-23"
      },
      {
        "vote_count": 224,
        "id": 2661,
        "video": false,
        "vote_average": 6.1,
        "title": "Batman",
        "popularity": 6.3489,
        "poster_path": "/udDVJXtAFsQ8DimrXkVFqy4DGEQ.jpg",
        "original_language": "en",
        "original_title": "Batman",
        "genre_ids": [
          10751,
          12,
          35,
          878,
          80
        ],
        "backdrop_path": "/5gcdof2PKH1emllBdN1VXU706IP.jpg",
        "adult": false,
        "overview": "The Dynamic Duo faces four super-villains who plan to hold the world for ransom with the help of a secret invention that instantly dehydrates people.",
        "release_date": "1966-07-30"
      },
      {
        "vote_count": 7932,
        "id": 272,
        "video": false,
        "vote_average": 7.5,
        "title": "Batman Begins",
        "popularity": 26.330012,
        "poster_path": "/dr6x4GyyegBWtinPBzipY02J2lV.jpg",
        "original_language": "en",
        "original_title": "Batman Begins",
        "genre_ids": [
          28,
          80,
          18
        ],
        "backdrop_path": "/65JWXDCAfwHhJKnDwRnEgVB411X.jpg",
        "adult": false,
        "overview": "Driven by tragedy, billionaire Bruce Wayne dedicates his life to uncovering and defeating the corruption that plagues his home, Gotham City.  Unable to work within the system, he instead creates a new identity, a symbol of fear for the criminal underworld - The Batman.",
        "release_date": "2005-06-10"
      },
      {
        "vote_count": 1635,
        "id": 414,
        "video": false,
        "vote_average": 5.2,
        "title": "Batman Forever",
        "popularity": 13.846626,
        "poster_path": "/eTMrHEhlFPHNxpqGwpGGTdAa0xV.jpg",
        "original_language": "en",
        "original_title": "Batman Forever",
        "genre_ids": [
          28,
          80,
          14
        ],
        "backdrop_path": "/gVTTtYgKIBurNvkGiGC6735b8y.jpg",
        "adult": false,
        "overview": "The Dark Knight of Gotham City confronts a dastardly duo: Two-Face and the Riddler. Formerly District Attorney Harvey Dent, Two-Face believes Batman caused the courtroom accident which left him disfigured on one side. And Edward Nygma, computer-genius and former employee of millionaire Bruce Wayne, is out to get the philanthropist; as The Riddler. Former circus acrobat Dick Grayson, his family killed by Two-Face, becomes Wayne's ward and Batman's new partner Robin.",
        "release_date": "1995-06-16"
      },
      {
        "vote_count": 107,
        "id": 408648,
        "video": false,
        "vote_average": 5.8,
        "title": "Batman and Harley Quinn",
        "popularity": 24.862574,
        "poster_path": "/uVdxoD9kn28qC8VQiVA6Uif1QHl.jpg",
        "original_language": "en",
        "original_title": "Batman and Harley Quinn",
        "genre_ids": [
          12,
          28,
          16
        ],
        "backdrop_path": "/mBzNVeSRX5LVCi7a3UrPeFWxRqw.jpg",
        "adult": false,
        "overview": "Batman and Nightwing are forced to team with the Joker's sometimes-girlfriend Harley Quinn to stop a global threat brought about by Poison Ivy and Jason Woodrue, the Floronic Man.",
        "release_date": "2017-08-14"
      },
      {
        "vote_count": 1837,
        "id": 364,
        "video": false,
        "vote_average": 6.6,
        "title": "Batman Returns",
        "popularity": 12.887128,
        "poster_path": "/ifzddUhnsTf1h6guBUKBlDwuS1t.jpg",
        "original_language": "en",
        "original_title": "Batman Returns",
        "genre_ids": [
          28,
          14
        ],
        "backdrop_path": "/wNIE5dpkiHU2csDRptMutFjAGiV.jpg",
        "adult": false,
        "overview": "Having defeated the Joker, Batman now faces the Penguin - a warped and deformed individual who is intent on being accepted into Gotham society. Crooked businessman Max Schreck is coerced into helping him become Mayor of Gotham and they both attempt to expose Batman in a different light. Selina Kyle, Max's secretary, is thrown from the top of a building and is transformed into Catwoman - a mysterious figure who has the same personality disorder as Batman. Batman must attempt to clear his name, all the time deciding just what must be done with the Catwoman.",
        "release_date": "1992-06-19"
      },
      {
        "vote_count": 1557,
        "id": 415,
        "video": false,
        "vote_average": 4.2,
        "title": "Batman & Robin",
        "popularity": 13.024827,
        "poster_path": "/79AYCcxw3kSKbhGpx1LiqaCAbwo.jpg",
        "original_language": "en",
        "original_title": "Batman & Robin",
        "genre_ids": [
          28,
          80,
          14
        ],
        "backdrop_path": "/h4tsxkwe0lRHBK1WHliNASK63IK.jpg",
        "adult": false,
        "overview": "Along with crime-fighting partner Robin and new recruit Batgirl, Batman battles the dual threat of frosty genius Mr. Freeze and homicidal horticulturalist Poison Ivy. Freeze plans to put Gotham City on ice, while Ivy tries to drive a wedge between the dynamic duo.",
        "release_date": "1997-06-20"
      },
      {
        "vote_count": 7637,
        "id": 209112,
        "video": false,
        "vote_average": 5.7,
        "title": "Batman v Superman: Dawn of Justice",
        "popularity": 96.439858,
        "poster_path": "/cGOPbv9wA5gEejkUN892JrveARt.jpg",
        "original_language": "en",
        "original_title": "Batman v Superman: Dawn of Justice",
        "genre_ids": [
          28,
          12,
          14
        ],
        "backdrop_path": "/vsjBeMPZtyB7yNsYY56XYxifaQZ.jpg",
        "adult": false,
        "overview": "Fearing the actions of a god-like Super Hero left unchecked, Gotham City’s own formidable, forceful vigilante takes on Metropolis’s most revered, modern-day savior, while the world wrestles with what sort of hero it really needs. And with Batman and Superman at war with one another, a new threat quickly arises, putting mankind in greater danger than it’s ever known before.",
        "release_date": "2016-03-23"
      },
      {
        "vote_count": 9,
        "id": 464882,
        "video": false,
        "vote_average": 8,
        "title": "Batman vs. Two-Face",
        "popularity": 22.144445,
        "poster_path": "/yTbj8Kcp4WFwMRdEejnn55KIsfH.jpg",
        "original_language": "en",
        "original_title": "Batman vs. Two-Face",
        "genre_ids": [
          28,
          16,
          35
        ],
        "backdrop_path": "/zuBoGr3DiGMjNC6W02YbtbeW0E1.jpg",
        "adult": false,
        "overview": "Former Gotham City District Attorney Harvey Dent, one side of his face scarred by acid, goes on a crime spree based on the number '2'. All of his actions are decided by the flip of a defaced, two-headed silver dollar.",
        "release_date": "2017-11-14"
      },
      {
        "vote_count": 1576,
        "id": 324849,
        "video": false,
        "vote_average": 7.2,
        "title": "The Lego Batman Movie",
        "popularity": 14.82077,
        "poster_path": "/snGwr2gag4Fcgx2OGmH9otl6ofW.jpg",
        "original_language": "en",
        "original_title": "The Lego Batman Movie",
        "genre_ids": [
          28,
          16,
          35,
          10751,
          14
        ],
        "backdrop_path": "/gbO4tWvMC0oLbNvHP9Zth3eUeAA.jpg",
        "adult": false,
        "overview": "In the irreverent spirit of fun that made “The Lego Movie” a worldwide phenomenon, the self-described leading man of that ensemble—Lego Batman—stars in his own big-screen adventure. But there are big changes brewing in Gotham, and if he wants to save the city from The Joker’s hostile takeover, Batman may have to drop the lone vigilante thing, try to work with others and maybe, just maybe, learn to lighten up.",
        "release_date": "2017-02-08"
      },
      {
        "vote_count": 199,
        "id": 366924,
        "video": false,
        "vote_average": 6.8,
        "title": "Batman: Bad Blood",
        "popularity": 9.893374,
        "poster_path": "/xp65MaH8bycr5v14f8jPO2NEXlX.jpg",
        "original_language": "en",
        "original_title": "Batman: Bad Blood",
        "genre_ids": [
          28,
          16,
          878
        ],
        "backdrop_path": "/1jpJpe5sx0AzSLOMFfds0s5alSw.jpg",
        "adult": false,
        "overview": "Bruce Wayne is missing. Alfred covers for him while Nightwing and Robin patrol Gotham City in his stead. And a new player, Batwoman, investigates Batman's disappearance.",
        "release_date": "2016-03-22"
      },
      {
        "vote_count": 511,
        "id": 382322,
        "video": false,
        "vote_average": 6.1,
        "title": "Batman: The Killing Joke",
        "popularity": 9.381561,
        "poster_path": "/zm0ODjtfJfJW0W269LqsQl5OhJ8.jpg",
        "original_language": "en",
        "original_title": "Batman: The Killing Joke",
        "genre_ids": [
          28,
          16,
          80,
          18
        ],
        "backdrop_path": "/iYrE2zV1DfZ3l7Lh4frHzNwFncq.jpg",
        "adult": false,
        "overview": "As Batman hunts for the escaped Joker, the Clown Prince of Crime attacks the Gordon family to prove a diabolical point mirroring his own fall into madness. Based on the graphic novel by Alan Moore and Brian Bolland.",
        "release_date": "2016-07-21"
      },
      {
        "vote_count": 65,
        "id": 20077,
        "video": false,
        "vote_average": 6.6,
        "title": "Batman vs Dracula",
        "popularity": 7.132916,
        "poster_path": "/vPr6t2stb4NJ54151ms5xu8bykP.jpg",
        "original_language": "en",
        "original_title": "Batman vs Dracula",
        "genre_ids": [
          14,
          16,
          27,
          28,
          53,
          878
        ],
        "backdrop_path": "/4aAlheGhU4rkDE4gSjekBnRXdGL.jpg",
        "adult": false,
        "overview": "Gotham City is terrorized not only by recent escapees Joker and Penguin, but by the original creature of the night, Dracula! Can Batman stop the ruthless vampire before he turns everyone in the city, including The Caped Crusader, Joker and Penguin, into his mindless minions?",
        "release_date": "2005-10-18"
      },
      {
        "vote_count": 167,
        "id": 22855,
        "video": false,
        "vote_average": 6.7,
        "title": "Superman/Batman: Public Enemies",
        "popularity": 8.227418,
        "poster_path": "/7eaHkUKAzfstt6XQCiXyuKiZUAw.jpg",
        "original_language": "en",
        "original_title": "Superman/Batman: Public Enemies",
        "genre_ids": [
          16,
          28,
          12
        ],
        "backdrop_path": "/aMyEMGuW2NV3A0whuEM7VKp6M73.jpg",
        "adult": false,
        "overview": "United States President Lex Luthor uses the oncoming trajectory of a Kryptonite meteor to frame Superman and declare a $1 billion bounty on the heads of the Man of Steel and his ‘partner in crime’, Batman. Heroes and villains alike launch a relentless pursuit of Superman and Batman, who must unite—and recruit help—to try and stave off the action-packed onslaught, stop the meteor Luthors plot.",
        "release_date": "2009-09-29"
      },
      {
        "vote_count": 229,
        "id": 321528,
        "video": false,
        "vote_average": 6.8,
        "title": "Batman vs. Robin",
        "popularity": 6.832488,
        "poster_path": "/6g7iQJAgyDn9mep98RXhLI64RcA.jpg",
        "original_language": "en",
        "original_title": "Batman vs. Robin",
        "genre_ids": [
          28,
          12,
          16
        ],
        "backdrop_path": "/oERfMmGc1GJRnC8IuFUH7zpR5z5.jpg",
        "adult": false,
        "overview": "Damian Wayne is having a hard time coping with his father's \"no killing\" rule. Meanwhile, Gotham is going through hell with threats such as the insane Dollmaker, and the secretive Court of Owls.",
        "release_date": "2015-04-14"
      },
      {
        "vote_count": 182,
        "id": 13851,
        "video": false,
        "vote_average": 6.6,
        "title": "Batman: Gotham Knight",
        "popularity": 7.980666,
        "poster_path": "/eIAhXROHG8t3QQ7qU0HfZgL5XFf.jpg",
        "original_language": "en",
        "original_title": "Batman: Gotham Knight",
        "genre_ids": [
          16,
          28,
          12
        ],
        "backdrop_path": "/cAW1NAahXhBUFJZgicBIGPs2OkZ.jpg",
        "adult": false,
        "overview": "Explore Bruce Wayne's transition from his beginning as a tormented vigilantee to The Dark Knight of a crumbling metropolis with six distinct chapters but intended to be viewed as a whole.",
        "release_date": "2008-07-03"
      },
      {
        "vote_count": 320,
        "id": 242643,
        "video": false,
        "vote_average": 7.3,
        "title": "Batman: Assault on Arkham",
        "popularity": 7.703079,
        "poster_path": "/nYOoqnGuiUUNE4gccO0JJigfACP.jpg",
        "original_language": "en",
        "original_title": "Batman: Assault on Arkham",
        "genre_ids": [
          53,
          16,
          28,
          80
        ],
        "backdrop_path": "/rZhblFe1Y9IheA47fO3i0b0qdvt.jpg",
        "adult": false,
        "overview": "Based on the hit video game series, Batman must find a bomb planted by the Joker while dealing with a mysterious team of villains called, The Suicide Squad.",
        "release_date": "2014-08-12"
      },
      {
        "vote_count": 265,
        "id": 69735,
        "video": false,
        "vote_average": 7.1,
        "title": "Batman: Year One",
        "popularity": 7.582468,
        "poster_path": "/bI1YVuhBN6Vws1GP9Mf01DyhC2s.jpg",
        "original_language": "en",
        "original_title": "Batman: Year One",
        "genre_ids": [
          28,
          12,
          16,
          80,
          878
        ],
        "backdrop_path": "/hAhMOPPPxzCKsCu5rUMYdwS8Yb5.jpg",
        "adult": false,
        "overview": "Two men come to Gotham City: Bruce Wayne after years abroad feeding his lifelong obsession for justice and Jim Gordon after being too honest a cop with the wrong people elsewhere. After learning painful lessons about the city's corruption on its streets and police department respectively, this pair learn how to fight back their own way. With that, Gotham's evildoers from top to bottom are terrorized by the mysterious Batman and the equally heroic Gordon is assigned to catch him by comrades who both hate and fear him themselves. In the ensuing manhunt, both find much in common as the seeds of an unexpected friendship are laid with additional friends and rivals helping to start the legend.",
        "release_date": "2011-09-27"
      },
      {
        "vote_count": 228,
        "id": 14919,
        "video": false,
        "vote_average": 7.5,
        "title": "Batman: Mask of the Phantasm",
        "popularity": 7.881964,
        "poster_path": "/l4jaQjkgznu2Rz05X18f24UjPNW.jpg",
        "original_language": "en",
        "original_title": "Batman: Mask of the Phantasm",
        "genre_ids": [
          28,
          12,
          16,
          10751
        ],
        "backdrop_path": "/jTwt4fcYADv758PVP6DH8ryy4R.jpg",
        "adult": false,
        "overview": "An old flame of Bruce Wayne's strolls into town, re-heating up the romance between the two. At the same time, a mass murderer with an axe for one hand begins systematically eliminating Gotham's crime bosses. Due to the person's dark appearance, he is mistaken for Batman. Now on the run, Batman must solve the mystery and deal with the romance between him and Andrea Beaumont.",
        "release_date": "1993-12-25"
      },
      {
        "vote_count": 11,
        "id": 125249,
        "video": false,
        "vote_average": 7,
        "title": "The Bat Man",
        "popularity": 2.482412,
        "poster_path": "/7wnRn8iQ0QInEK1CaZFqw1zPhht.jpg",
        "original_language": "en",
        "original_title": "The Bat Man",
        "genre_ids": [
          28
        ],
        "backdrop_path": "/xEG5iP1qZCiDt4BefSpLy1d54zE.jpg",
        "adult": false,
        "overview": "Japanese master spy Daka operates a covert espionage-sabotage organization located in Metropolis' now-deserted Little Tokyo, which turns American scientists into pliable zombies.",
        "release_date": "1943-07-16"
      }
    ]
  }

}
