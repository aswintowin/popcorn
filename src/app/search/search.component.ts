import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import {SearchService} from './../../services/search.service';
import {ActivityService} from './../../services/activity.service'
import {IMovie} from '../../dto/Movie';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.css']
})
export class SearchComponent implements OnInit {
    movies: IMovie[];
    name: string;
    content: string;
    searchString: any;

  constructor(private searchService: SearchService, private activityService: ActivityService, private route: ActivatedRoute, private router: Router) {  }

  ngOnInit() {   
    this.route.queryParams
    .subscribe(params => {
      this.searchString = params['search'];
      this.loadPage(this.searchString);
    });
  }

  loadPage(value:string) {
    this.searchService.searchMovie(this.searchString)
    .subscribe(movies => {
        this.movies = movies.results;
    });
  }

  search(event){
      this.searchService.searchMovie(this.name)
      .subscribe(movies => {
          this.movies = movies.results;
          this.content = "search"
      })
  }

  addWatched(movie){
      console.log(movie);
      this.activityService.addMovie(movie.title, movie.id, movie.backdrop_path, 1)
      .subscribe(res => console.log(res));
  }

  addFavorite(movie){
    this.activityService.addMovie(movie.title, movie.id, movie.backdrop_path, 2)
    .subscribe(res =>res => console.log(res));
  }

}
