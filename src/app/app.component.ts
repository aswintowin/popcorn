import { Component } from '@angular/core';
import { Router} from "@angular/router";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  query: string;

  constructor(private router: Router) {  }

  search(){
    this.router.navigate(['/search'], { queryParams: { search: this.query } });
    // this.router.navigate(['/search/12345'], { queryParams: { search: this.name } });
  }

}
