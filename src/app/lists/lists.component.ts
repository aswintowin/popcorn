import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-lists',
  templateUrl: './lists.component.html',
  styleUrls: ['./lists.component.css']
})
export class ListsComponent implements OnInit {
  lists : any;
  listname: string;
  constructor() { }

  ngOnInit() {
    this.loadLists();
  }

  loadLists(){
    this.lists = [
      {
        id: 2345,
        name: 'Watched',
        count: 25,
        type: 'basic'
      },
      {
        id: 2346,
        name: 'Favorite',
        count: 5,
        type: 'basic'
      },
      {
        id: 2347,
        name: 'Wishlist',
        count: 102,
        type: 'basic'
      }
    ]
  }

  addNewList(event){
    this.lists = [
      {
        id: 2345,
        name: 'Watched',
        count: 25,
        type: 'basic'
      },
      {
        id: 2346,
        name: 'Favorite',
        count: 5,
        type: 'basic'
      },
      {
        id: 2347,
        name: 'Wishlist',
        count: 102,
        type: 'basic'
      },
      {
        id: 2348,
        name: this.listname,
        count: 0,
        type: 'custom'
      }
    ]

  }

  deleteList(listname){
    console.log("dsfsdfsdf");
  }
}
