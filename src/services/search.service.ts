import {Injectable} from '@angular/core';
import {Inject} from '@angular/core';
import {Http, Headers} from '@angular/http';
import 'rxjs/add/operator/map';

@Injectable()
export class SearchService{
    constructor( @Inject(Http) private http:Http){
        this.http = http;
        console.log('Search Service Initialized...');
    }

    getTredningMovies(){
        return this.http.get('https://api.themoviedb.org/3/discover/movie?api_key=6d3b69e7a8589ed8558a414d661e7ee1&language=en-US&sort_by=popularity.desc&include_adult=false&include_video=false&page=1&year=2017')
        .map(res => res.json());        
    }

    getTredningShows(){
        return this.http.get('https://api.themoviedb.org/3/discover/tv?api_key=6d3b69e7a8589ed8558a414d661e7ee1&language=en-US&sort_by=popularity.desc&include_adult=false&include_video=false&page=1&year=2017')
        .map(res => res.json());        
    }

    searchMovie(searchText){
        return this.http.get('https://api.themoviedb.org/3/search/movie?api_key=6d3b69e7a8589ed8558a414d661e7ee1&language=en-US&query='+searchText+'&page=1&include_adult=false')
        .map(res => res.json());
    }

    getMovieDetails(movieId){
        return this.http.get('https://api.themoviedb.org/3/movie/'+ movieId +'?api_key=6d3b69e7a8589ed8558a414d661e7ee1&language=en-US')
        .map(res => res.json());
    }

    getMovieCredits(movieId){
        return this.http.get('https://api.themoviedb.org/3/movie/'+ movieId +'/credits?api_key=6d3b69e7a8589ed8558a414d661e7ee1')
        .map(res => res.json());
    }

    getPersonDetails(personId){
        return this.http.get('https://api.themoviedb.org/3/person/'+ personId +'?api_key=6d3b69e7a8589ed8558a414d661e7ee1&language=en-US')
        .map(res => res.json());
    }

    getPersonCredits(persionId){
        return this.http.get('https://api.themoviedb.org/3/person/'+ persionId +'/movie_credits?api_key=6d3b69e7a8589ed8558a414d661e7ee1')
        .map(res => res.json());
    }
}