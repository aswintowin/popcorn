import {Injectable} from '@angular/core';
import {Inject} from '@angular/core';
import {Http, Headers} from '@angular/http';
import 'rxjs/add/operator/map';

@Injectable()
export class ActivityService{
    constructor( @Inject(Http) private http:Http){
        this.http = http;
    }

    addMovie(title, id, backdrop_path, type){
        var movie = {
            title: title,
            id: id,
            backdrop_path: backdrop_path,
            type: type,
            createdOn: new Date().toLocaleString()     
        };
        
        var headers = new Headers();
        headers.append('Content-Type', 'application/json');
    
        return this.http.post(' http://localhost:3000/api/movie', JSON.stringify(movie), {headers: headers})
            .map(res => res.json());
    }

    deleteMovie(id){
        return this.http.delete('/api/movie/'+id)
        .map(res => res.json());
    }

    updateMovie(movie){
        var headers = new Headers();
        headers.append('Content-Type', 'application/json');
    
        return this.http.put('/api/movie/'+movie._id, JSON.stringify(movie), {headers: headers})
            .map(res => res.json());
    }
}