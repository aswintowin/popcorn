export interface ICredit{
    id: number;
    cast: [{
        cast_id: number;
        character: string;
        credit_id: string;
        id: number;
        name: String;
        order: number;
        profile_path: string;
    }
    ];
    crew: [{
        credit_id: string;
        department: string;
        id: number;
        job: string;
        name: string;
        profile_path: string;
    }];
}